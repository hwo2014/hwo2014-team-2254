package hwo14.bigobot.ingame;

public class Race {
    private int laps;
    private int mxLapTime;

    public Race(int laps, int mxLapTime) {
        this.laps = laps;
        this.mxLapTime = mxLapTime;
    }

    public int getLaps() {
        return laps;
    }

    public int getMxLapTime() {
        return mxLapTime;
    }
}
