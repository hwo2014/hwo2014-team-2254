package hwo14.bigobot.ingame;

public class StraightTrackPiece extends TrackPiece {
    protected double length;

    public StraightTrackPiece(int id, double length, boolean swiitch) {
        super(id);
        this.length = length;
        this.swiitch = swiitch;
    }

    @Override
    public double getLength(int lane) {
        return length;
    }

    @Override
    public boolean isArc() {
        return false;
    }

    @Override
    public String toString() {
        return "Index " + getIndex() + " : StraightTrackPiece {length=" + length + '}';
    }
}
