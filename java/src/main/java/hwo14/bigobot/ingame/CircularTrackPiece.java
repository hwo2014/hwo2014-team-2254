package hwo14.bigobot.ingame;

public class CircularTrackPiece extends TrackPiece {
    protected double radius;
    protected double angle;

    public CircularTrackPiece(int id, double radius, double angle, boolean swiitch) {
        super(id);
        this.radius = radius;
        this.angle = Math.toRadians(angle);
        this.swiitch = swiitch;
    }

    @Override
    public double getLength(int laneDis) {
        return Math.abs(angle * (radius - laneDis));  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isArc() {
        return true;
    }

    public double getRadius() {
        return radius;
    }

    public double getAngle() {
        return angle;
    }

    @Override
    public String toString() {
        return "Index " + getIndex() + " : CircularTrackPiece {radius=" + radius + ", angle=" + angle + '}';
    }
}
