package hwo14.bigobot.ingame;

public abstract class TrackPiece {

    protected int index;
    protected boolean swiitch;

    public TrackPiece(int id) {
        this.index = id;
    }

    public abstract double getLength(int laneDis);

    public boolean hasSwitch() {
        return swiitch;
    }

    public abstract boolean isArc();

    public int getIndex() {
        return index;
    }
}
