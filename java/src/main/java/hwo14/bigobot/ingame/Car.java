package hwo14.bigobot.ingame;

public class Car {
    private String name;
    private String color;

    private double length;
    private double width;
    private double flagPosition;

    private double speed;
    private double acceleration;
    private double angle;

    private int pieceId;
    private double inPieceDistant;
    private int laneFrom;
    private int laneTo;

    private int currentLap;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getFlagPosition() {
        return flagPosition;
    }

    public void setFlagPosition(double flagPosition) {
        this.flagPosition = flagPosition;
    }

    public int getPieceId() {
        return pieceId;
    }

    public void setPieceId(int pieceId) {
        this.pieceId = pieceId;
    }

    public double getInPieceDistant() {
        return inPieceDistant;
    }

    public void setInPieceDistant(double inPieceDistant) {
        this.inPieceDistant = inPieceDistant;
    }

    public int getLaneFrom() {
        return laneFrom;
    }

    public void setLaneFrom(int laneFrom) {
        this.laneFrom = laneFrom;
    }

    public int getLaneTo() {
        return laneTo;
    }

    public void setLaneTo(int laneTo) {
        this.laneTo = laneTo;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public int getCurrentLap() {
        return currentLap;
    }

    public void setCurrentLap(int currentLap) {
        this.currentLap = currentLap;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(double acceleration) {
        this.acceleration = acceleration;
    }
}
