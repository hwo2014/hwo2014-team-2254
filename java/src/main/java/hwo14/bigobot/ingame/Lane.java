package hwo14.bigobot.ingame;

public class Lane {

    private int distant;
    private int index;
    private int fromLeft;

    public Lane(int distant, int index) {
        this.distant = distant;
        this.index = index;
    }

    public int getDistant() {
        return distant;
    }

    public int getIndex() {
        return index;
    }

    public int getFromLeft() {
        return fromLeft;
    }

    public void setFromLeft(int fromLeft) {
        this.fromLeft = fromLeft;
    }
}
