package hwo14.bigobot;

import hwo14.bigobot.ingame.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class Engine {
    Logger log = LoggerFactory.getLogger(Engine.class);

    private int tick;

    private List<TrackPiece> trackPieces;
    private Map<String, Car> cars;
    private List<Lane> lanes;
    private MessageSender sender;
    private boolean active;
    private Race race;

    private Map<TrackPiece, SwitchDirection> switchActions;
    private Map<TrackPiece, Double> maxSpeeds;

    private int leftLaneOffset = 0;
    private int rightLaneOffset = 0;

    private String ourCarId;
    private Car ourCar;

    private double deltaAngle;

    private double throttle = 0.0;

    public Engine(MessageSender sender) {
        this.sender = sender;
    }

    public String getOurCarId() {
        return ourCarId;
    }

    public void setOurCarId(String ourCarId) {
        this.ourCarId = ourCarId;
    }

    public void initTracks(List<TrackPiece> trackPieces) {
        this.trackPieces = trackPieces;
        log.info("Track initialized");
        for (int i = 0; i < trackPieces.size(); i++) {
            log.info(trackPieces.get(i).toString());
        }
    }

    public void initCars(List<Car> cars) {
        this.cars = new HashMap<String, Car>();
        for (Car car : cars) {
            this.cars.put(car.getName(), car);
        }

        if (ourCarId != null) {
            for (Car car : cars) {
                if (car.getName().equals(ourCarId)) {
                    ourCar = car;
                    log.info("Our car is " + ourCarId);
                }
            }

        }
        log.info("Cars initialized");
    }

    public void initLanes(List<Lane> lanes) {
        this.lanes = lanes;
        Comparator<Lane> comp = new Comparator<Lane>() {
            @Override
            public int compare(Lane o1, Lane o2) {
                return o1.getDistant() - o2.getDistant();
            }
        };
        Collections.sort(lanes, comp);
        for (int i = 0; i < lanes.size(); i++) {
            lanes.get(i).setFromLeft(i);
        }
        leftLaneOffset = lanes.get(0).getDistant();
        rightLaneOffset = lanes.get(lanes.size() - 1).getDistant();
        comp = new Comparator<Lane>() {
            @Override
            public int compare(Lane o1, Lane o2) {
                return o1.getIndex() - o2.getIndex();
            }
        };
        Collections.sort(lanes, comp);
        log.info("Lanes initialized");
        calculateSwitches();
        calculateMaxSpeeds();
    }

    private void calculateSwitches() {
        switchActions = new HashMap<>();
        int currentTrackId = 0;
        TrackPiece currentTrack = trackPieces.get(currentTrackId);
        double leftDis = 0;
        double rightDis = 0;
        TrackPiece prevSwitch = null;
        int safetyCount = 3 * trackPieces.size();
        while (!switchActions.containsKey(currentTrack) && safetyCount > 0) {
            if (currentTrack.hasSwitch()) {
                if (prevSwitch == null) {
                    prevSwitch = currentTrack;
                } else {
                    if (leftDis > rightDis) {
                        switchActions.put(prevSwitch, SwitchDirection.Right);
                        log.info("@ Switch " + prevSwitch.getIndex() + " try turn right");
                    } else if (leftDis < rightDis) {
                        switchActions.put(prevSwitch, SwitchDirection.Left);
                        log.info("@ Switch " + prevSwitch.getIndex() + " try turn left");

                    }
                    prevSwitch = currentTrack;
                }
            } else {
                leftDis += currentTrack.getLength(leftLaneOffset);
                rightDis += currentTrack.getLength(rightLaneOffset);
            }

            currentTrackId = (currentTrackId + 1) % trackPieces.size();
            currentTrack = trackPieces.get(currentTrackId);
            safetyCount--;
        }
        if (prevSwitch != null && !switchActions.containsKey(prevSwitch)) {
            if (leftDis > rightDis) {
                switchActions.put(prevSwitch, SwitchDirection.Right);
                log.info("@ Switch " + prevSwitch.getIndex() + " try turn right");
            } else if (leftDis < rightDis) {
                switchActions.put(prevSwitch, SwitchDirection.Left);
                log.info("@ Switch " + prevSwitch.getIndex() + " try turn left");
            }
        }
    }

    private void calculateMaxSpeeds(){
        maxSpeeds = new HashMap<>();
        for (int i = 0; i < trackPieces.size(); i++) {
            if(trackPieces.get(i) instanceof CircularTrackPiece){
                CircularTrackPiece trackPiece = (CircularTrackPiece) trackPieces.get(i);
                double speed = calculateInitSpeed(trackPiece.getRadius(), trackPiece.getAngle());
                maxSpeeds.put(trackPiece, speed);
                log.info("Setting track "+trackPiece.getIndex()+" speed to "+ speed);
            }
        }
        boolean changed = true;
        while (changed){
            changed = false;
            Map<TrackPiece,Double> tempMap = new HashMap<>();
            for (Map.Entry<TrackPiece, Double> trackPieceSpeedEntry : maxSpeeds.entrySet()) {
                TrackPiece prev = trackPieces.get((trackPieceSpeedEntry.getKey().getIndex()+trackPieces.size()-1)%trackPieces.size());
                double prevLength = Math.min(prev.getLength(leftLaneOffset),prev.getLength(rightLaneOffset));
                double prevSpeed = Math.sqrt(trackPieceSpeedEntry.getValue()*trackPieceSpeedEntry.getValue()+2*0.1*prevLength);
                if(maxSpeeds.containsKey(prev) && maxSpeeds.get(prev)<=prevSpeed){
                    continue;
                } else {
                    tempMap.put(prev,prevSpeed);
                    log.info("Setting track "+prev.getIndex()+" speed to "+ prevSpeed);
                    changed = true;
                }
            }
            maxSpeeds.putAll(tempMap);
        }
    }

    public void setRace(Race race) {
        this.race = race;
    }

    public void start() {
        active = true;
        throttle = 1.0;
        trackPieceChanged(0);
        sender.throttle(throttle);
    }

    public void onCrash(String carId) {
        if (ourCarId.equals(carId)) {
            active = false;
        }
        log.info("Our car is crashed");
        TrackPiece atPiece = trackPieces.get(ourCar.getPieceId());
        if (atPiece.isArc()) {
            CircularTrackPiece circularTrackPiece = (CircularTrackPiece) atPiece;
            log.info("Arc radius=" + circularTrackPiece.getRadius() + " , angle=" + circularTrackPiece.getAngle());
            log.info("Car speed=" + ourCar.getSpeed() + " , angle=" + ourCar.getAngle() + " , IPDis=" + ourCar.getInPieceDistant() + " , accel=" + ourCar.getAcceleration());
        }
    }

    public void onSpawn(String carId) {
        if (ourCarId.equals(carId)) {
            active = true;
            trackPieceChanged(ourCar.getPieceId());
            throttle = 1.0;
            sender.throttle(throttle);
        }
        log.info("Our car is now ready to run");
    }

    public void updateCar(String id, double angle, int piece, double inPieceDis, int laneFrom, int laneTo, int lap,int newTick) {
        Car car = cars.get(id);
        double speed;
        double acc;
        int deltaTick = 1;
        if(tick>=0){
            deltaTick = newTick-this.tick;
        }
        this.tick = newTick;
        if(deltaTick>0){
            if (car.getPieceId() == piece) {
                speed = (inPieceDis - car.getInPieceDistant())/deltaTick; //assuming this is done per tick
                acc = (speed - car.getSpeed())/deltaTick;
            } else {
                speed = inPieceDis + trackPieces.get(car.getPieceId()).getLength(lanes.get(laneFrom).getDistant()) - car.getInPieceDistant(); //assuming this is done per tick
                speed /= deltaTick;
                acc = (speed - car.getSpeed())/deltaTick;
            }
            car.setSpeed(speed);
            car.setAcceleration(acc);
        }
        if (car.equals(ourCar) && car.getPieceId() != piece) {
            trackPieceChanged(piece);
        }
        angle = Math.toRadians(angle);
        deltaAngle = angle - car.getAngle();
        car.setAngle(angle);
        car.setPieceId(piece);
        car.setInPieceDistant(inPieceDis);
        car.setLaneFrom(laneFrom);
        car.setLaneTo(laneTo);
        car.setCurrentLap(lap);

        if (car.equals(ourCar)) {
            if (active) {
//                log.info("update: speed="+speed+" , acc="+acc+" , angle="+angle+" , lap="+lap+" ("+inPieceDis+"m@"+piece+") , lane="+laneFrom);
            }
            sendResponse();
        }
    }

    private void trackPieceChanged(int newId) {
        log.info("Moving to piece " + newId);
        int nextId = (newId + 1) % trackPieces.size();
        TrackPiece nextPiece = trackPieces.get(nextId);
        if (nextPiece.hasSwitch() && switchActions.containsKey(nextPiece)) {
            if (switchActions.get(nextPiece) == SwitchDirection.Left && ourCar.getLaneTo() > 0) {
                sender.switchLane(SwitchDirection.Left);
                log.info("Switching left @ " + nextId);
                return;
            }
            if (switchActions.get(nextPiece) == SwitchDirection.Right && ourCar.getLaneTo() < lanes.size() - 1) {
                sender.switchLane(SwitchDirection.Right);
                log.info("Switching right @ " + nextId);
                return;
            }
        }
    }

    private void sendResponse() {
        if (!active) {
            sender.ping();
            return;
        }
        TrackPiece currentPiece = trackPieces.get(ourCar.getPieceId());

        double remaining = currentPiece.getLength(lanes.get(ourCar.getLaneFrom()).getDistant())-ourCar.getInPieceDistant();
        if (currentPiece.isArc()) {
            CircularTrackPiece trackPiece = (CircularTrackPiece) currentPiece;
            double speed = calculateInitSpeed(trackPiece.getRadius(),trackPiece.getAngle());
            speed = Math.sqrt(speed*speed-2*0.1*Math.min(ourCar.getInPieceDistant(),trackPiece.getRadius()*Math.PI/2));

            int next = (currentPiece.getIndex()+1)%trackPieces.size();
            double endSpeed = maxSpeeds.get(trackPieces.get(next));

            endSpeed = Math.sqrt(endSpeed*endSpeed+2*0.1*remaining);

            speed = Math.min(speed,endSpeed);

            if (ourCar.getSpeed() > speed || Math.abs(ourCar.getAngle())>0.85) {
                log.info("In arc: [brake] current speed=" + ourCar.getSpeed() + " limit="+speed);
                throttle = 0.0;
            }  else {
                throttle = 1.0;
                log.info("In arc: [accel] current speed=" + ourCar.getSpeed() + " limit="+speed);
            }
        } else {
            int next = (currentPiece.getIndex()+1)%trackPieces.size();
            double speed = maxSpeeds.get(trackPieces.get(next));
            speed = Math.sqrt(speed*speed+2*0.1*remaining);

            if (ourCar.getSpeed() > speed) {
                throttle = 0.0;
                log.info("In line: [brake] current speed=" + ourCar.getSpeed() + " limit="+speed);
            } else {
                throttle = 1.0;
                log.info("In line: [accel] current speed=" + ourCar.getSpeed() + " limit="+speed);
            }
        }
        sender.throttle(throttle);

        if (ourCar.getLaneFrom() != ourCar.getLaneTo()) {
            log.info("Changing track from " + ourCar.getLaneFrom() + " to " + ourCar.getLaneTo());
        }
        return;
    }

    private double calculateInitSpeed(double radius, double angle){
        angle = Math.abs(angle);
        double magicConstant = 0.2916;
        double speed = Math.sqrt(magicConstant*radius);
//        if(angle<Math.PI/4){
//            speed = Math.sqrt(speed * speed +2*0.1*radius*(Math.PI/2-angle));
//        }else {
//            speed = Math.sqrt(speed * speed +2*0.1*radius*Math.PI/4);
//        }
        speed = Math.sqrt(speed * speed +2*0.1*radius*Math.PI/4);
        return speed;
    }
}
