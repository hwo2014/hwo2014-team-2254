package hwo14.bigobot;

import hwo14.bigobot.ingame.SwitchDirection;
import hwo14.bigobot.msg.JoinMsg;
import hwo14.bigobot.msg.PingMsg;
import hwo14.bigobot.msg.SwitchMsg;
import hwo14.bigobot.msg.ThrottleMsg;

import java.io.PrintWriter;

public class MessageSender {

    private PrintWriter writer;

    public MessageSender(PrintWriter writer) {
        this.writer = writer;
    }

    public void join(String name, String key) {
        JoinMsg msg = new JoinMsg(name, key);
        send(msg.toJson());
    }

    public void throttle(double value) {
        ThrottleMsg msg = new ThrottleMsg(value);
        send(msg.toJson());
    }

    public void switchLane(SwitchDirection direction) {
        SwitchMsg msg = new SwitchMsg(direction);
        send(msg.toJson());
    }

    public void ping() {
        PingMsg msg = new PingMsg();
        send(msg.toJson());
    }

    public void customMsg(String msg) {
        send(msg);
    }

    private void send(String msg) {
        writer.println(msg);
        writer.flush();
    }
}
