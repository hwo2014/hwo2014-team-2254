package hwo14.bigobot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;
import hwo14.bigobot.msg.*;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, botName, botKey);
    }



    public Main(final BufferedReader reader, final PrintWriter writer, final String botName, final String botKey) throws IOException {

        MessageSender sender = new MessageSender(writer);
        sender.join(botName,botKey);
//        String germany = "{\"msgType\": \"joinRace\", \"data\": { \"botId\": { \"name\": \""+botName+"\", \"key\": \""+botKey+"\" }, \"trackName\": \"germany\", \"carCount\": 1 }}";
//        sender.customMsg(germany);
//        String usa = "{\"msgType\": \"joinRace\", \"data\": { \"botId\": { \"name\": \""+botName+"\", \"key\": \""+botKey+"\" }, \"trackName\": \"usa\", \"carCount\": 1 }}";
//        sender.customMsg(usa);
        Engine engine = new Engine(sender);
        MessageReceiver receiver = new MessageReceiver(reader,engine);
        receiver.start();
    }

}
