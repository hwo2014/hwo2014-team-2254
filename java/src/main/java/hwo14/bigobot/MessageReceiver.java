package hwo14.bigobot;

import com.google.gson.Gson;
import hwo14.bigobot.ingame.*;
import hwo14.bigobot.msg.MsgWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MessageReceiver {

    Logger log = LoggerFactory.getLogger(MessageReceiver.class);

    private boolean listen;
    private BufferedReader reader;
    private final Gson gson = new Gson();
    private Engine engine;

    public MessageReceiver(BufferedReader reader, Engine engine) {
        this.reader = reader;
        this.engine = engine;
    }

    public void start() {
        listen = true;
        String line;

        try {
            while (listen && (line = reader.readLine()) != null) {
                final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
                if (msgFromServer.msgType.equals("carPositions")) {
                    Map<String,Object> map = gson.fromJson(line,Map.class);
                    int tick = -1;
                    if(map.containsKey("gametick")){
                        tick = ((Double)map.get("gametick")).intValue();
                    }
                    carPositions(msgFromServer.data,tick);
                } else if (msgFromServer.msgType.equals("joinMsg")) {
                    joinAck(msgFromServer.data);
                } else if (msgFromServer.msgType.equals("yourCar")) {
                    yourCar(msgFromServer.data);
                } else if (msgFromServer.msgType.equals("gameInit")) {
                    gameInit(msgFromServer.data);
                } else if (msgFromServer.msgType.equals("gameEnd")) {
                    gameEnd(msgFromServer.data);
                } else if (msgFromServer.msgType.equals("gameStart")) {
                    gameStart(msgFromServer.data);
                } else if (msgFromServer.msgType.equals("tournamentEnd")) {
                    tournamentEnd(msgFromServer.data);
                } else if (msgFromServer.msgType.equals("crash")) {
                    crash(msgFromServer.data);
                } else if (msgFromServer.msgType.equals("spawn")) {
                    spawn(msgFromServer.data);
                } else if (msgFromServer.msgType.equals("lapFinished")) {
                    lapFinished(msgFromServer.data);
                } else if (msgFromServer.msgType.equals("dnf")) {
                    dnf(msgFromServer.data);
                } else if (msgFromServer.msgType.equals("finish")) {
                    finish(msgFromServer.data);
                } else {
//                    send(new PingMsg());
                }
            }
        } catch (IOException e) {
            log.error("Error reading input from server", e);
        }


    }

    public void stop() {
        listen = false;
    }

    public void joinAck(Object data) {
        log.info("Joined the server successfully");
    }

    public void yourCar(Object data) {
        Map<String, Object> dataMap = (Map<String, Object>) data;
        String ourId = (String) dataMap.get("name");
        engine.setOurCarId(ourId);
    }

    public void gameInit(Object data) {
        Map<String, Object> dataMap = (Map<String, Object>) data;
        Map<String, Object> race = (Map<String, Object>) dataMap.get("race");
        Map<String, Object> track = (Map<String, Object>) race.get("track");
        List<Object> pieces = (List<Object>) track.get("pieces");
        List<TrackPiece> trackPieces = new ArrayList<TrackPiece>(pieces.size());
        for (int i = 0; i < pieces.size(); i++) {
            Map<String, Object> piece = (Map<String, Object>) pieces.get(i);
            boolean hasSwitch = piece.containsKey("switch") && (Boolean) piece.get("switch");
            if (piece.containsKey("length")) {
                trackPieces.add(new StraightTrackPiece(i, (Double) piece.get("length"), hasSwitch));
            } else if (piece.containsKey("radius")) {
                trackPieces.add(new CircularTrackPiece(i, (Double) piece.get("radius"), (Double) piece.get("angle"), hasSwitch));
            }
        }
        engine.initTracks(trackPieces);

        List<Object> lanes = (List<Object>) track.get("lanes");
        List<Lane> laneList = new ArrayList<Lane>(lanes.size());
        for (int i = 0; i < lanes.size(); i++) {
            Map<String, Object> lane = (Map<String, Object>) lanes.get(i);
            laneList.add(new Lane(((Double) lane.get("distanceFromCenter")).intValue(), ((Double) lane.get("index")).intValue()));
        }
        engine.initLanes(laneList);

        List<Object> cars = (List<Object>) race.get("cars");
        List<Car> carList = new ArrayList<Car>(cars.size());
        for (int i = 0; i < cars.size(); i++) {
            Car car = new Car();
            Map<String, Object> carOb = (Map<String, Object>) cars.get(i);
            Map<String, Object> carIdInfo = (Map<String, Object>) carOb.get("id");
            car.setName((String) carIdInfo.get("name"));
            car.setColor((String) carIdInfo.get("color"));
            Map<String, Object> dimensionInfo = (Map<String, Object>) carOb.get("dimensions");
            car.setLength((Double) dimensionInfo.get("length"));
            car.setWidth((Double) dimensionInfo.get("width"));
            car.setFlagPosition((Double) dimensionInfo.get("guideFlagPosition"));
            carList.add(car);
        }
        engine.initCars(carList);

//        Map<String, Object> raceSession = (Map<String, Object>) race.get("raceSession");
//        Race raze = new Race(((Double) raceSession.get("laps")).intValue(), ((Double) raceSession.get("maxLapTimeMs")).intValue());
//        engine.setRace(raze);
    }

    public void gameStart(Object data) {
        engine.start();
    }

    public void carPositions(Object data, int tick) {
        List<Map<String, Object>> positions = (List<Map<String, Object>>) data;
        for (Map<String, Object> position : positions) {
            Map<String, Object> carId = (Map<String, Object>) position.get("id");
            String carName = (String) carId.get("name");
            double angle = (Double) position.get("angle");
            Map<String, Object> piecePosition = (Map<String, Object>) position.get("piecePosition");
            int pieceId = ((Double) piecePosition.get("pieceIndex")).intValue();
            double inPieceDis = (Double) piecePosition.get("inPieceDistance");
            Map<String, Object> laneInfo = (Map<String, Object>) piecePosition.get("lane");
            int fromLane = ((Double) laneInfo.get("startLaneIndex")).intValue();
            int toLane = ((Double) laneInfo.get("endLaneIndex")).intValue();
            int lap = ((Double) piecePosition.get("lap")).intValue();
            engine.updateCar(carName, angle, pieceId, inPieceDis, fromLane, toLane, lap,tick);
        }

    }

    public void gameEnd(Object data) {
        log.info("Game end");
    }

    public void tournamentEnd(Object data) {
        log.info("tournament end");
    }

    public void crash(Object data) {
        log.info("crash");
        Map<String, Object> crashInf = (Map<String, Object>) data;
        engine.onCrash((String) crashInf.get("name"));
    }

    public void spawn(Object data) {
        log.info("spawn");
        Map<String, Object> spawnInfo = (Map<String, Object>) data;
        engine.onSpawn((String) spawnInfo.get("name"));
    }

    public void lapFinished(Object data) {
        log.info("lap end");
    }

    /**
     * called on disqualification
     */
    public void dnf(Object data) {
        log.info("dnf");
    }

    /**
     * Called on any car finishing race
     */
    public void finish(Object data) {
        log.info("finish");
    }
}
