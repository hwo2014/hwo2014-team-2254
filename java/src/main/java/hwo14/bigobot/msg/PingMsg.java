package hwo14.bigobot.msg;

public class PingMsg extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}
