package hwo14.bigobot.msg;

import hwo14.bigobot.ingame.SwitchDirection;

public class SwitchMsg extends SendMsg {
    private SwitchDirection direction;

    public SwitchMsg(SwitchDirection direction){
        this.direction = direction;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }

    @Override
    protected Object msgData() {
        return direction;
    }

}
