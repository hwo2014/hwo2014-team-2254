package hwo14.bigobot.msg;

public class ThrottleMsg extends SendMsg {
    private double value;

    public ThrottleMsg(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}
